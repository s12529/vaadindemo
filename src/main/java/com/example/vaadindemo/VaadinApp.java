package com.example.vaadindemo;

import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	

	@Override
	protected void init(VaadinRequest request) {
/*		
		VerticalLayout vl = new VerticalLayout();
		
		final Label helloLabel = new Label("Hello World Today!");
		final TextField helloTF = new TextField();
		Button helloBtn = new Button("Say Hello");
		
		vl.addComponent(helloTF);
		vl.addComponent(helloLabel);
		vl.addComponent(helloBtn);
		
		setContent(vl);
		
		
		helloBtn.addClickListener( new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				helloLabel.setValue(helloTF.getValue());
				
			}
		});
		*/
		
		GridLayout digs = new GridLayout(4,3);
		
		final TextField firstNumber = new TextField();
		final TextField secondNumber = new TextField();
		final Button multiplyBtn = new Button("X");
		final Button addBtn = new Button("+");
		final Button divideBtn = new Button("/");
		final Label resultLabel = new Label("Nothing here.");
		
		digs.addComponent(firstNumber,0,1);
		digs.addComponent(multiplyBtn,1,0);
		digs.addComponent(addBtn,1,1);
		digs.addComponent(divideBtn,1,2);
		digs.addComponent(secondNumber,2,1);
		digs.addComponent(resultLabel,3,1);

		setContent(digs);
		divideBtn.addClickListener( new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {

				double x = Double.valueOf(firstNumber.getValue());
				double y = Double.valueOf(secondNumber.getValue());
				double r = x / y;
				String res = Double.toString(r);

				resultLabel.setValue(res);
				
			}
		});
		
		addBtn.addClickListener( new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				double x = Double.valueOf(firstNumber.getValue());
				double y = Double.valueOf(secondNumber.getValue());
				double r = x + y;
				String res = Double.toString(r);

				resultLabel.setValue(res);
			}
		});
		
		
		multiplyBtn.addClickListener( new ClickListener() {
			
		
			@Override
			public void buttonClick(ClickEvent event) {
				
				double x = Double.valueOf(firstNumber.getValue());
				double y = Double.valueOf(secondNumber.getValue());
				double r = x * y;
				String res = Double.toString(r);

				resultLabel.setValue(res);
			}
		});
		
		
		
		
		
	} 
	
}
